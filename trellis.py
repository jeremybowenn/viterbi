import numpy as np
import copy


def viterbi(evidence_vector, prior, states, evidence_variables, transition_probs, emission_probs):
    """
    40 points
    This method takes as input the following:
    evidence_vector: A list of dictionaries mapping evidence variables to their values
    prior: A dictionary corresponding to the prior distribution over states
    states: A list of all possible system states
    evidence_variables: A list of all valid evidence variables
    transition_probs: A dictionary mapping states onto dictionaries mapping states onto probabilities
    emission_probs: A dictionary mapping states onto dictionaries mapping evidence variables onto
                probabilities for their possible values

    This method returns:
        A list of states that is the most likely sequence of states explaining the evidence

    """

    # TODO: implement me!
    states_array = []
    max_prob = 0
    max_state = states[0]
    for i in states:
        state_prob = 0.
        for j in prior:
            # print(emission_probs[j]['Note'][evidence_vector[0]['Note']])
            state_prob += prior[j]*transition_probs[i][j]*emission_probs[j]['Note'][evidence_vector[0]['Note']]

        # print(state_prob)
        if state_prob >= max_prob:
            max_prob = state_prob
            max_state = i

    # print(max_prob, max_state)
    states_array.append(max_state)

    for i in evidence_vector[1:]:
        max_prob = 0
        max_state = states[0]
        for j in states:
            # print(transition_probs[states_array[len(states_array)-1]][j], emission_probs[j]['Note'][i['Note']], j)
            state_prob = transition_probs[states_array[len(states_array)-1]][j]*emission_probs[j]['Note'][i['Note']]
            # print(state_prob)
            if state_prob >= max_prob:
                max_prob = state_prob
                max_state = j
        # print(max_prob, max_state, "Done")
        states_array.append(max_state)

    # print(states_array)

    return states_array
